<div id="page-wrapper" class="page-wrapper-cls">
<div class="alert alert-info"><h3 align="center"><i class="fa fa-newspaper-o"></i> LAPORAN BERITA </h3></div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>LOKASI</th>
                        <th>JUDUL BERITA</th>
                        <th>REP</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    include "../../koneksi.php";
                    $cek=mysqli_query($connect, "SELECT * FROM hasil");
                    while ($data=mysqli_fetch_array($cek)) { 
                        $data_berita=$data['id_berita']; 
                        $cek_berita=mysqli_query($connect,"SELECT * FROM berita WHERE id_berita='$data_berita'");
                        while ($data_brt=mysqli_fetch_array($cek_berita)) { ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data_brt['lokasi'];?></td>
                        <td><?php echo $data_brt['judul'];?></td>
                        <td><?php echo $data_brt['rep'];?></td>
                        <td><?php $status=$data['status'];
                        if($status=='1') { echo "Tayang"; } else { echo "Tidak Tayang"; } ?></td>
                    </tr>
                <?php $no++;
                } } ?>
                </tbody>
            </table>
        </div>
    </div>
<form>
<input type="button" value="Cetak Halaman Ini" onClick="window.print()" class="btn btn-sm btn-primary" />
</form>
<hr>
</div>