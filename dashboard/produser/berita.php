<div id="page-wrapper" class="page-wrapper-cls">
<?php
$act=(isset($_GET['act']) ? strtolower($_GET['act']) : NULL);//$_GET[act];
if ($act=='change') {
$id=$_GET['id_berita'];
    ?>
<div class="alert alert-warning"><h3 align="center"><i class="fa fa-pencil"></i> UBAH KETENTUAN NASKAH </h3></div>
    <div class="col-md-6 col-md-offset-3">
    <form enctype="multipart/form-data" method="POST" action="">
        <div class="form-group">
            <label>Naskah</label>
            <select class="form-control" name="naskah">
                <option value="4">---- Pilih ----</option>
                <option value="4">5W + 1 H (Sangat Baik)</option>
                <option value="3">4W + 1 H (Baik)</option>
                <option value="2">3 W + 1 H (Kurang Baik)</option>
                <option value="1">2 W + 1 H (Tidak Baik)</option>
            </select>
        </div>
        <div class="form-group">
            <label>Kerapian Video</label>
            <select class="form-control" name="video">
                <option value="3">---- Pilih ----</option>
                <option value="3">Rapi</option>
                <option value="2">Kurang Rapi</option>
                <option value="1">Tidak Rapi</option>
            </select>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a class="btn btn-danger" HREF="index.php?menu=berita">KEMBALI</a>
                <input type="submit" name="update" class="btn btn-primary" value="UPDATE">
            </div>
        </div>
</div>
<?php 
    if(isset($_POST['update'])){
    $id_berita  = $_GET['id_berita'];
    $naskah     = $_POST['naskah'];
    if ($naskah=="1") {
        $n      = "0.5,0.5,0";
    }
    elseif ($naskah=="2") {
        $n      = "0,1,0";
    }
    elseif ($naskah=="3") {
        $n      = "0,0.5,0.5";
    }
    else {
        $n      = "0,0,1";
    }

    $video      = $_POST['video'];
     if ($video=="1") {
        $v      = "0,0,1";
    }
    elseif ($video=="2") {
        $v      = "0,1,0";
    }
    else {
        $v      = "1,0,0";
    }
    mysqli_query($connect,"DELETE FROM hasil WHERE id_berita='$id'");
    $hasil = mysqli_query($connect,"UPDATE konversi_vektor SET naskah_v='$n', video_v='$v' WHERE id_berita='$id'");
        if ($hasil) {
          echo '<script language="javascript">alert("Success"); document.location="index.php?menu=berita";</script>';
        }
        else {
          echo '<script language="javascript">alert("Gagal"); document.location="index.php?menu=berita";</script>';
        }
    }
}
else { ?>
<div class="alert alert-info"><h3 align="center"><i class="fa fa-newspaper-o"></i> DATA BERITA </h3></div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>LOKASI</th>
                        <th>JUDUL BERITA</th>
                        <th>REP / KAM</th>
                        <th>DUR</th>
                        <th>SIFAT</th>
                        <th>WCR</th>
                        <th>NASKAH</th>
                        <th>VIDEO</th>
                        <th>PENGIRIM</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    include "../../koneksi.php";
                    $cek=mysqli_query($connect, "SELECT * FROM berita");
                    while ($data=mysqli_fetch_array($cek)) { 
                        $id=$data['id_berita'];
                        $cek_b=mysqli_query($connect,"SELECT * FROM konversi_vektor WHERE id_berita='$id' AND naskah_v!='0,0,0' AND video_v!='0,0,0'");
                        $hit=mysqli_num_rows($cek_b);
                        ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['lokasi'];?></td>
                        <td><?php if ($hit > 0) { echo "<a title='Data ini sudah pernah dilakukan perhitungan sebelumnya.'>".$data['judul']."</a>"; } else { echo $data['judul']; } ?></td>
                        <td><?php echo $data['rep'];?></td>
                        <td><?php echo $data['dur'];?> Sekon</td>
                        <td><?php echo $data['id_sub'];?></td>
                        <td><?php echo $data['wcr'];?> Orang</td>
                        <td><center><a href="../../assets/berkas_upload/naskah/<?php echo $data['naskah'];?>" target="_BLANK" class="btn btn-sm btn-success" title="Download File "><i class="fa fa-download"></i></a></center></td>
                        <td><center><a href="../../assets/berkas_upload/video/<?php echo $data['video'];?>" target="_BLANK" class="btn btn-sm btn-success" title="Download File "><i class="fa fa-download"></i></a></center></td>
                        <td><?php $id=$data['id_user']; 
                            $dataaa=mysqli_query($connect,"SELECT * FROM user WHERE id_user='$id'");
                            $user=mysqli_fetch_array($dataaa);
                            echo $user['nama_lengkap'];
                        ?></td>
                        <td><center>
                            <?php if ($hit > 0) { ?>
                            <a href="index.php?menu=berita&act=change&id_berita=<?php echo $data['id_berita'];?>" class="btn btn-sm btn-warning" onclick="javascript:return confirm('Data sudah dinilai. Ingin menilai kembali?')"><i class="fa fa-pencil"></i></a>
                        <?php } else { ?>
                            <a href="index.php?menu=berita&act=change&id_berita=<?php echo $data['id_berita'];?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                        <?php } ?>
                        </center></td>
                    </tr>
                <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>
</div>