<div id="page-wrapper" class="page-wrapper-cls">
<?php
$act=(isset($_GET['act']) ? strtolower($_GET['act']) : NULL);

if ($act=='proses_iterasi') { 

//PERHITUNGAN FUZZY
mysqli_query($connect, "DELETE FROM konversi_real");
mysqli_query($connect, "DELETE FROM hasil");
$cek = mysqli_query($connect,"SELECT * FROM konversi_vektor");
while ($data=mysqli_fetch_array($cek)) {
    $id_konversi_vektor=$data['id_konversi_vektor'];
    $id_berita=$data['id_berita'];
    $n=$data['naskah_v'];
    $e_n=explode(",", $n);
    $w=$data['wcr_v'];
    $e_w=explode(",", $w);
    $d=$data['durasi_v'];
    $e_d=explode(",", $d);
    $v=$data['video_v'];
    $e_v=explode(",", $v);
    $i_n=implode(",",$e_n);
    $i_w=implode(",",$e_w);
    $i_d=implode(",",$e_d);
    $i_v=implode(",",$e_v);
    $arrayhasil=$i_n.",".$i_d.",".$i_w.",".$i_v;
    mysqli_query($connect,"INSERT INTO konversi_real (naskah) values ('$arrayhasil')");
    mysqli_query($connect,"INSERT INTO hasil (id_konversi_vektor,id_berita) values ('$id_konversi_vektor','$id_berita')");

    $data2=array(0,0,1,0,1,0.5,0,1,1,0,0);
    $a=mysqli_query($connect, "SELECT * FROM konversi_real");
    $count2=count($data2);
    while ($b=mysqli_fetch_array($a)) {
        $db=$b['naskah'];
        $c=explode(",", $db);
        $hasil=array();
        for ($i=0;$i<1;$i++) {
            for ($j=0;$j<$count2;$j++) {
                $hasil[]=$data2[$j]*$c[$j];
                $hasil2=$data2[$j]*$c[$j];
            }
            $implode=implode(",", $hasil);
            $defuzzy=array_sum($hasil);
            if ($defuzzy<=2) { $status = 0;  }
            else { $status= 1;}
            mysqli_query($connect,"UPDATE hasil SET nilai_defuzzy='$defuzzy',status='$status' WHERE id_berita='$id_berita'");
        }
    }
}
mysqli_query($connect, "DELETE FROM konversi_real");
?>
<div class="alert alert-info"><h3 align="center"><i class="fa fa-newspaper-o"></i> HASIL ANALISA BERITA </h3></div>
    <div class="panel-body">
        <a href="index.php?menu=proses" class="btn btn-sm btn-danger">KEMBALI</a>
        <a href="index.php?menu=proses&act=iterasi" class="btn btn-sm btn-primary">LIHAT PROSES ITERASI</a>
        <hr>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>JUDUL BERITA</th>
                        <th>NILAI DEFUZZYFIKASI</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    include "../../koneksi.php";
                    $cek=mysqli_query($connect, "SELECT * FROM hasil");
                    while ($data=mysqli_fetch_array($cek)) { 
                        $data_berita=$data['id_berita']; 
                        $cek_berita=mysqli_query($connect,"SELECT * FROM berita WHERE id_berita='$data_berita'");
                        while ($data_brt=mysqli_fetch_array($cek_berita)) { ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data_brt['judul'];?></td>
                        <td><?php echo $data['nilai_defuzzy'];?></td>
                        <td><?php $status=$data['status'];
                        if($status=='1') { echo "Tayang"; } else { echo "Tidak Tayang"; } ?></td>
                    </tr>
                <?php $no++;
                } } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php }
elseif ($act=='iterasi') { 
$cek    = mysqli_query($connect,"SELECT * FROM berita");
$hitung = mysqli_num_rows($cek);
$nomor  = $hitung+1;

// END
// STEP PERHITUNGAN MANUAL
?>
<div class="alert alert-info"><h3 align="center"><i class="fa fa-newspaper-o"></i> STEP BY STEP PERHITUNGAN </h3></div>
    <div class="panel-body">
        <a href="index.php?menu=proses&act=proses_iterasi" class="btn btn-sm btn-danger">KEMBALI</a>
        <hr>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <tr><td colspan='<?php echo $nomor; ?>'><center><b>VEKTOR A</b></center></td></tr>
                <?php
                // PENYAJIAN VEKTOR A
                $no=1;
                $cek=mysqli_query($connect,"SELECT * FROM konversi_vektor");
                while ($data=mysqli_fetch_array($cek)) {
                    echo "<tr><td>";
                    echo "<b>A".$no."</b>";
                    echo "</td><td>";
                    echo $data['naskah_v'];
                    echo "</td><td>";
                    echo $data['durasi_v'];
                    echo "</td><td>";
                    echo $data['wcr_v'];
                    echo "</td><td>";
                    echo $data['video_v'];
                    echo "</td></tr>";
                $no++; }
                ?>
            </table>
            <table class="table table-striped table-bordered table-hover">
            <tr><td colspan='<?php echo $nomor; ?>'><center><b>VEKTOR B</b></center></td></tr>
            <?php
            $nomor  = $hitung+1;for ($a=0;$a<$hitung;$a++) {
                $nomor=$a+1;
                echo "<tr><td>";
                echo "<b>B".$nomor."</b>";
               for ($b=0;$b<$hitung;$b++) {
                echo "</td><td>";
                    if ($a==$b) {
                        echo 1;
                    }
                    else {
                        echo 0;
                    }
                }
                echo "</td></tr>";
            }
            echo "</table>"; 
            $nomor  = $hitung+1;
            echo "</table>"; ?>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NILAI DEFUZZYFIKASI</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    include "../../koneksi.php";
                    $cek=mysqli_query($connect, "SELECT * FROM hasil");
                    while ($data=mysqli_fetch_array($cek)) { 
                        $data_berita=$data['id_berita']; 
                        $cek_berita=mysqli_query($connect,"SELECT * FROM berita WHERE id_berita='$data_berita'");
                        while ($data_brt=mysqli_fetch_array($cek_berita)) { ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['nilai_defuzzy'];?></td>
                        <td><?php $status=$data['status'];
                        if($status=='1') { echo "Tayang"; } else { echo "Tidak Tayang"; } ?></td>
                    </tr>
                <?php $no++;
                } } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php }
else { ?>
<div class="alert alert-info"><h3 align="center"><i class="fa fa-newspaper-o"></i> PROSES DATA BERITA </h3></div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>JUDUL BERITA</th>
                        <th>REP</th>
                        <th>DURASI</th>
                        <th>SIFAT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    include "../../koneksi.php";
                    $cek=mysqli_query($connect, "SELECT * FROM berita");
                    while ($data=mysqli_fetch_array($cek)) { ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['judul'];?></td>
                        <td><?php echo $data['rep'];?></td>
                        <td><?php echo $data['dur'];?> Sekon</td>
                        <td><?php echo $data['id_sub'];?></td>
                    </tr>
                <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <a href="index.php?menu=proses&act=proses_iterasi" class="btn btn-sm btn-primary">PROSES BERITA</a>
        <hr>
    </div>
<?php } ?>
</div>