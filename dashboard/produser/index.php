<?php
session_start();
if(!isset($_SESSION['id_user'])){
echo"<script>document.location.href='../../index.php'</script>";
}   
else {
include "../../koneksi.php";
$q=mysqli_query($connect, "SELECT * FROM user where id_user='".$_SESSION['id_user']."'");
while($d=mysqli_fetch_array($q))
    {
      $id_user=$d['id_user'];
      $nama=$d['nama_lengkap'];
      $username=$d['username'];
      //$last_login=$d['last_login'];
}
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Dashboard Produser | Fuzzy Associative Memory</title>
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav  class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="../../assets/img/user.png" class="img-circle" />
                        </div>
                    </li>
                    <li><a href="index.php?menu=home"><i class="fa fa-home"></i>Beranda</a></li>
                    <li><a href="index.php?menu=kelola_user"><i class="fa fa-users"></i>Kelola User</a></li>
                    <li><a href="index.php?menu=berita"><i class="fa fa-newspaper-o"></i>Berita Masuk</a></li>
                    <li><a href="index.php?menu=proses"><i class="fa fa-refresh"></i>Proses Berita</a></li>
                    <li><a href="index.php?menu=laporan"><i class="fa fa-book"></i>Laporan</a></li>
                    <li><a href="logout.php"><i class="fa fa-sign-out"></i>Logout</a></li>
                </ul>
            </div>
        </nav>
                <?php
                if (isset($_GET['menu'])){
                    $menu=$_GET['menu'];
                    if($menu=="home"){include "home.php";} 
                    if($menu=="berita"){include "berita.php";} 
                    if($menu=="proses"){include "proses.php";} 
                    if($menu=="laporan"){include "laporan.php";} 
                    if($menu=="kelola_user"){include "kelola_user.php";} 
                    if($menu=="ubah_pass"){include "ubah_pass.php";}}
                else{include"home.php";}
                ?>
        </div>
    <footer><center>© 2018 - Fuzzy Associative Memory. All Rights Reserved | Developer By : Nanda</center></footer>
    <script src="../../assets/js/jquery-1.11.1.js"></script>
    <script src="../../assets/js/bootstrap.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php } ?>