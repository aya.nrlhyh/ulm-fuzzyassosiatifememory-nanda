<div id="page-wrapper" class="page-wrapper-cls">
    <div class="row">
        <div class="alert alert-info"><h3 align="center">WELCOME, PRODUSER </h3></div>
        <div class="col-md-8">
                <center><!--<h5>Login Terakhir : <?php echo date('h:m:s , d-m-Y',strtotime($last_login));?><br> --><h6>(Silahkan Kelola Data - Data Anda, Jangan Lupa <b>Logout</b> Setelah Menggunakan Aplikasi Ini)</h6</h5><br>
                <small><i class="fa fa-check"></i> You Logged As Produser</small><hr>
                <img src="../../assets/img/logo.png" width="70%" />
                <h3 align="center"><b>SELAMAT DATANG DI APLIKASI</b></h3>
                <p>IMPLEMENTASI FUZZY ASSOCIATIVE<br>MEMORY UNTUK PENDUKUNG KEPUTUSAN<br>KELAYAKAN TAYANG BERITA <br>
                (STUDI KASUS : TVRI)</p><center><hr>           
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h2 align="center"><b>Informasi</b></h2></div>
                <div class="panel-body">
                    <h4><center><b>Ketentuan Menggunakan Aplikasi</b></center></h4>
                    <hr />
                    <ul>
                        <li>Anda memiliki akses login sebagai Produser</li>
                        <li>Anda bisa mengoreksi berita yang dikirimkan melalui menu "Berita Masuk"</li>
                        <li>Menu "Proses Data" untuk melakukan proses data berita apakah berita layak ditayangkan atau tidak</li>
                        <li>Untuk keluar dari system silahkan klik menu "Logout"</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>