<?php
session_start();
if(!isset($_SESSION['id_user'])){
echo"<script>document.location.href='../../index.php'</script>";
}   
else {
include "../../koneksi.php";
$q=mysqli_query($connect, "SELECT * FROM user where id_user='".$_SESSION['id_user']."'");
while($d=mysqli_fetch_array($q))
    {
      $id_user=$d['id_user'];
      $nama=$d['nama_lengkap'];
      $username=$d['username'];
}
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Dashboard Kontributor | Fuzzy Associative Memory</title>
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav  class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="../../assets/img/user.png" class="img-circle" />
                        </div>
                    </li>
                    <li><a href="index.php?menu=home"><i class="fa fa-home"></i>Beranda</a></li>
                    <li><a href="index.php?menu=berita"><i class="fa fa-newspaper-o"></i>Data Berita</a></li>
                    <li>
                        <a href="#"><i class="fa fa-book"></i> Informasi Berita <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li><a href="index.php?menu=tayang&hasil=1"><i class="fa fa-check"></i>Tayang</a></li>
                            <li><a href="index.php?menu=tayang&hasil=0"><i class="fa fa-times"></i>Tidak Tayang</a></li>
                        </ul>
                    </li>
                    <li><a href="logout.php"><i class="fa fa-sign-out"></i>Logout</a></li>
                </ul>
            </div>
        </nav>
                <?php
                if (isset($_GET['menu'])){
                    $menu=$_GET['menu'];
                    if($menu=="home"){include "home.php";} 
                    if($menu=="berita"){include "berita.php";}
                    if($menu=="tayang"){include "tayang.php";} 
                    if($menu=="ubah_pass"){include "ubah_pass.php";}}
                else{include"home.php";}
                ?>
        </div>
    <footer><center>© 2018 - Fuzzy Associative Memory. All Rights Reserved | Developer By : Nanda</center></footer>
    <script src="../../assets/js/jquery-1.11.1.js"></script>
    <script type='text/javascript'>
        function showonlyone(thechosenone) {
              var newboxes = document.getElementsByTagName("div");
                    for(var x=0; x<newboxes.length; x++) {
                          name = newboxes[x].getAttribute("name");
                          if (name == 'newboxes') {
                                if (newboxes[x].id == thechosenone) {
                                newboxes[x].style.display = 'block';
                          }
                          else {
                                newboxes[x].style.display = 'none';
                          }
                    }
              }
        }
    </script>
    <script src="../../assets/js/bootstrap.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php } ?>