<div id="page-wrapper" class="page-wrapper-cls">
<?php
$hasil=$_GET['hasil'];
if ($hasil==1) { ?>
<div class="alert alert-info"><h3 align="center"><i class="fa fa-newspaper-o"></i> BERITA YANG BERHASIL DI TAYANGKAN </h3></div>
    <div class="panel-body">
        <div class="table-responsive"> <small>
<input type="button" value="Cetak Halaman Ini" onClick="window.print()" class="btn btn-sm btn-primary" /></small><br><br>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>LOKASI</th>
                        <th>JUDUL BERITA</th>
                        <th>REP / KAM</th>
                        <th>KEPENTINGAN</th>
                        <th>KEINDAHAN</th>
                        <th>AKURAT</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    include "../../koneksi.php";
                    $cek=mysqli_query($connect, "SELECT * FROM hasil WHERE status='1'");
                    while ($data=mysqli_fetch_array($cek)) {
                    $id=$data['id_berita'];
                        $ceklagi=mysqli_query($connect,"SELECT * FROM berita WHERE id_berita='$id' AND id_user='$id_user'");
                        while ($berita=mysqli_fetch_array($ceklagi)) { ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $berita['lokasi'];?></td>
                        <td><?php echo $berita['judul'];?></td>
                        <td><?php echo $berita['rep'];?></td>
                        <td><center><?php $sub=$berita['id_sub']; 
                        $ceksub=mysqli_query($connect,"SELECT * FROM sub_sifat_berita WHERE id_sub='$sub'");
                        $datasub=mysqli_fetch_array($ceksub);
                        $sifat=$datasub['id_sifat'];
                        $nama_sifat=$datasub['nama_sub_sifat'];
                        if ($sifat==1) {echo $nama_sifat; } else { echo " - "; } ?></center></td>
                        <td><center><?php if ($sifat==2) {echo $nama_sifat; } else { echo  " - "; } ?></center></td>
                        <td><center><?php if($data['status']=='1') { echo "Aktual/Faktual"; } else { echo "Kadaluarsa"; } ?></center></td>
                        <td><button class="btn btn-sm btn-success"> Tayang </button></td>
                    </tr>
                <?php $no++; }
                } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php }
elseif ($hasil==0) { ?>
<div class="alert alert-info"><h3 align="center"><i class="fa fa-newspaper-o"></i> BERITA YANG TIDAK LAYAK DITAYANGKAN </h3></div>
    <div class="panel-body">
        <div class="table-responsive"> <small><a class="btn btn-sm btn-primary" href=""><i class="fa fa-print"></i> Cetak Laporan </a></small><br><br>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>LOKASI</th>
                        <th>JUDUL BERITA</th>
                        <th>REP / KAM</th>
                        <th>KEPENTINGAN</th>
                        <th>KEINDAHAN</th>
                        <th>AKURAT</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    include "../../koneksi.php";
                    $cek=mysqli_query($connect, "SELECT * FROM hasil WHERE status='0'");
                    while ($data=mysqli_fetch_array($cek)) {
                    $id=$data['id_berita'];
                        $ceklagi=mysqli_query($connect,"SELECT * FROM berita WHERE id_berita='$id' AND id_user='$id_user'");
                        while ($berita=mysqli_fetch_array($ceklagi)) { ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $berita['lokasi'];?></td>
                        <td><?php echo $berita['judul'];?></td>
                        <td><?php echo $berita['rep'];?></td>
                        <td><center><?php $sub=$berita['id_sub']; 
                        $ceksub=mysqli_query($connect,"SELECT * FROM sub_sifat_berita WHERE id_sub='$sub'");
                        $datasub=mysqli_fetch_array($ceksub);
                        $sifat=$datasub['id_sifat'];
                        $nama_sifat=$datasub['nama_sub_sifat'];
                        if ($sifat==1) {echo $nama_sifat; } else { echo " - "; } ?></center></td>
                        <td><center><?php if ($sifat==2) {echo $nama_sifat; } else { echo " - "; } ?></center></td>
                        <td><center><?php if($data['status']=='1') { echo "Aktual/Faktual"; } else { echo "Kadaluarsa"; } ?></center></td>
                        <td><button class="btn btn-sm btn-danger"> Tidak Tayang </button></td>
                    </tr>
                <?php $no++; } 
            }
            ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>
</div>