<div id="page-wrapper" class="page-wrapper-cls">
    <div class="row">
        <div class="alert alert-info"><h3 align="center">WELCOME, KONTRIBUTOR </h3></div>
        <div class="col-md-8">
                <!--<h5>Login Terakhir : <?php //echo date('h:m:s , d-m-Y',strtotime($last_login));?><br>--><center><h6>(Silahkan Kelola Data - Data Anda, Jangan Lupa <b>Logout</b> Setelah Menggunakan Aplikasi Ini)</h6</h5><br>
                <small><i class="fa fa-check"></i> You Logged As Kontributor</small><hr>
                <img src="../../assets/img/logo.png" width="70%" />
                <h3 align="center"><b>SELAMAT DATANG DI APLIKASI</b></h3>
                <p>IMPLEMENTASI FUZZY ASSOCIATIVE<br>MEMORY UNTUK PENDUKUNG KEPUTUSAN<br>KELAYAKAN TAYANG BERITA <br>
                (STUDI KASUS : TVRI)</p><center><hr>           
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h2 align="center"><b>Informasi</b></h2></div>
                <div class="panel-body">
                    <h4><center><b>Ketentuan Menggunakan Aplikasi</b></center></h4>
                    <hr />
                    <ul>
                        <li>Anda memiliki akses login sebagai kontributor (Pengirim Berita)</li>
                        <li>Anda bisa menambahkan / mengirimkan berita melalui menu "Data Berita"</li>
                        <li>Menu "Informasi Berita" memiliki sub menu yaitu : <b>Tayang</b> (Data berita yang telah diproses dan layak ditayangkan) & <b>Tidak Tayang</b> (Data berita yang telah diproses dan tidak layak ditayangkan)</li>
                        <li>Untuk keluar dari system silahkan klik menu "Logout"</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>