<?php 
$act=(isset($_GET['act']) ? strtolower($_GET['act']) : NULL);//$_GET[act];
if($act=='reset_password'){    ?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Change Password | Fuzzy Associative Memory</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="assets/js/jquery-2.1.1.min.js"></script> 
    <link href="assets/css/font-awesome.css" rel="stylesheet"> 
</head>
<body>  
<div class="col-md-4 col-md-offset-4" style="margin-top:80px;">
    <div class="panel panel-primary" style="padding: 10%">
        <div class="panel-heading"><h2 align="center"><b>- CHANGE -</b></h3></div>
            <div class="panel-body">
                <form role="form" method="post" action="">
                    <div class="form-group">
                        <label class="control-label">PASSWORD BARU</label>
                        <input type="text" class="form-control" name="password" placeholder="Silahkan Isi Password Baru Anda" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" name="change" value="UBAH PASSWORD">
                        <small><a href="index.php">Kembali Ke Halaman Utama</a></small>
                    </div>
                    <?php
                    include "koneksi.php";
                    if(isset($_POST['change'])){
                          $id_user  = $_GET['id_user'];
                          $pw       = $_POST['password'];
                            $hasil = mysqli_query($connect,"UPDATE user SET password='$pw' WHERE id_user='$id_user'");
                            if ($hasil) {
                              echo '<script language="javascript">alert("Success"); document.location="index.php";</script>';
                            }
                            else {
                              echo '<script language="javascript">alert("Gagal"); document.location="index.php";</script>';
                            }
                        } 
                        ?>
                </form>
                <hr><h4 align="center">&copy; 2018 - Fuzzy Associative Memory<br><small>Developed By : Nanda</h2>
            </div>
    </div>
</div>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/bootstrap.js"> </script>
</body>
</html>
<?php 

} else { ?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Reset Password | Fuzzy Associative Memory</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="assets/js/jquery-2.1.1.min.js"></script> 
    <link href="assets/css/font-awesome.css" rel="stylesheet"> 
</head>
<body>  
<div class="col-md-4 col-md-offset-4" style="margin-top:80px;">
    <div class="panel panel-primary" style="padding: 10%">
        <div class="panel-heading"><h2 align="center"><b>- RESET PASSWORD -</b></h3></div>
            <div class="panel-body">
                <form role="form" method="post" action="action_reset.php">
                    <div class="form-group">
                        <label class="control-label">USERNAME</label>
                        <input type="text" class="form-control" name="username" placeholder="Silahkan Isi Username" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="LANJUTKAN">
                        <small><a href="index.php">Kembali Ke Halaman Utama</a></small>
                    </div>
                </form>
                <hr><h4 align="center">&copy; 2018 - Fuzzy Associative Memory<br><small>Developed By : Nanda</h2>
            </div>
    </div>
</div>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/bootstrap.js"> </script>
</body>
</html>

<?php
}
?>