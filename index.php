<!DOCTYPE HTML>
<html>
<head>
    <title>Login | Fuzzy Associative Memory</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="assets/js/jquery-2.1.1.min.js"></script> 
    <link href="assets/css/font-awesome.css" rel="stylesheet"> 
</head>
<body>	
<div class="col-md-4 col-md-offset-4" style="margin-top:80px;">
    <div class="panel panel-primary" style="padding: 10%">
        <div class="panel-heading"><h2 align="center"><b>- LOGIN ACCOUNT -</b></h3></div>
            <div class="panel-body">
                <form role="form" method="post" action="cek_login.php">
                    <div class="form-group">
                        <label class="control-label">USERNAME</label>
                        <input type="text" class="form-control" name="username" placeholder="Isikan Username" />
                    </div>
            	    <div class="form-group">
                        <label class="control-label">PASSWORD</label>
                        <input type="password" class="form-control" name="password" placeholder="*********************" />
                    </div>
                    <div class="form-group">
                    	<input type="submit" class="btn btn-primary" value="LOGIN">
                        <small><a href="reset.php">Anda lupa password? Klik disini</a></small>
                    </div>
                </form>
                <hr><h4 align="center">&copy; 2018 - Fuzzy Associative Memory<br><small>Developed By : Nanda</h2>
            </div>
    </div>
</div>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/bootstrap.js"> </script>
</body>
</html>