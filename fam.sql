-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2019 at 12:47 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fam`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(4) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `rep` varchar(100) NOT NULL,
  `dur` int(4) NOT NULL,
  `id_sub` varchar(100) NOT NULL,
  `naskah` varchar(100) NOT NULL,
  `wcr` int(2) NOT NULL,
  `video` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `status` double NOT NULL DEFAULT '1',
  `id_user` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `lokasi`, `judul`, `rep`, `dur`, `id_sub`, `naskah`, `wcr`, `video`, `tanggal`, `status`, `id_user`) VALUES
(1, 'BJM', 'TIM GABUNGAN GAGALKAN  NARKOBA JARINGAN MALAYSIA', 'DINA/SYAHRIL', 120, '1', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(2, 'BJM', 'LPG 3 KG PERUNTUKKANNYA BANYAK SALAH SASARAN', 'DINA/SYAHRIL', 120, '2', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(3, 'BJM', '8 PANGKALAN NAKAL DI CABUT IZIN', 'RINI/TEN', 130, '1', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(4, 'HST', 'REGROUPING SEKOLAH KEMBALI DITOLAK WARGA', 'FADLY', 120, '5', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(5, 'HST', 'DPRD HST TANGGAPI PENOLAKAN WARGA TERHADAP REGROUPING SEKOLAH', 'FADLY', 120, '5', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(6, 'BJM', '2 BUAH RUMAH DI LALAP SI JAGO MERAH', 'YANDA', 120, '6', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(7, 'TAPIN', 'PANEN PADI DI KABUPATEN TAPIN', 'IBNU SINA', 120, '10', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(8, 'BJM', 'OPREASI KESELAMATAN 2018', 'DEWI/SYAHRIL', 120, '1', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(9, 'BJM', 'POLRES HSU GELAR UPACARA OPS KESELAMATAN INTAN 2018', 'DONI', 120, '12', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(10, 'BJM', 'FORUM PERANGKAT DAERAH', 'RIWANDI/BUDI', 120, '16', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(11, 'BJM', 'LAYANAN KTP GRATIS DISAMBUT ANTUSIAS WARHA', 'RIWANDI/BUDI', 120, '16', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(12, 'BJM', '2 TAHUN KEPEMIMPINAN IBNU HERMAN DI MATA MEREKA', 'RINI/TEN', 120, '2', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(13, 'HST', 'KPPN BARABAI BANGUN ZONA INTEGRIAS BEBAS KORUPSI', 'FADLY', 120, '2', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(14, 'HSS', 'PENGAMBILAN SUMPAH KETUA DPRD HSS', 'IWAN SANUSI', 120, '2', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(15, 'BJM', 'KOMUNITAS PELANGI', 'DEWI/TEN', 120, '2', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(16, 'BJB', 'SISI BAHU KIRI-KANAN JALAN TRIKORA AMBLAS AKIBAT DIGERUS ARUS HUJAN DERAS', 'NURDIN YUSUF', 120, '1', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(17, 'BJM', '250 M SASIRANGAN TARGET', 'DEWI/TEN', 180, '2', 'A.JPG', 3, 'A.JPG', '2018-09-18', 1, 1),
(18, 'BJM', 'JASA PENITIPAN HEWAN MENINGKAT JELANG AKHIR TAHUN', 'ZULKARNAIN', 180, '1', 'A.JPG', 1, 'A.JPG', '2018-09-18', 1, 1),
(19, 'BJM', 'PASLON BUPATI TALA PUNYA CARA BARU REBUT SUARA RAKYAT', 'ZAINAL', 120, '16', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1),
(20, 'HSS', 'MUSDA LPTQ HSS 2018', 'IWAN', 120, '16', 'A.JPG', 2, 'A.JPG', '2018-09-18', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hasil`
--

CREATE TABLE `hasil` (
  `id_hasil` int(4) NOT NULL,
  `id_konversi_vektor` int(4) NOT NULL,
  `id_berita` int(4) NOT NULL,
  `nilai_defuzzy` double NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil`
--

INSERT INTO `hasil` (`id_hasil`, `id_konversi_vektor`, `id_berita`, `nilai_defuzzy`, `status`) VALUES
(274, 1, 1, 4, '1'),
(275, 2, 2, 4, '1'),
(276, 3, 3, 3.4166666666667, '1'),
(277, 4, 4, 4, '1'),
(278, 5, 5, 2.5, '1'),
(279, 6, 6, 2.5, '1'),
(280, 7, 7, 3.5, '1'),
(281, 8, 8, 3.5, '1'),
(282, 9, 9, 3.5, '1'),
(283, 10, 10, 3.5, '1'),
(284, 11, 11, 4, '1'),
(285, 12, 12, 3.5, '1'),
(286, 13, 13, 3, '1'),
(287, 14, 14, 3.5, '1'),
(288, 15, 15, 4, '1'),
(289, 16, 16, 3.5, '1'),
(290, 17, 17, 4, '1'),
(291, 18, 18, 0.5, '0'),
(292, 19, 19, 1.5, '0'),
(293, 20, 20, 1.5, '0');

-- --------------------------------------------------------

--
-- Table structure for table `konversi_real`
--

CREATE TABLE `konversi_real` (
  `id_konversi_real` int(4) NOT NULL,
  `id_berita` int(4) NOT NULL,
  `naskah` varchar(200) NOT NULL,
  `durasi` text NOT NULL,
  `wawancara` double NOT NULL,
  `video` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `konversi_vektor`
--

CREATE TABLE `konversi_vektor` (
  `id_konversi_vektor` int(4) NOT NULL,
  `id_berita` int(4) NOT NULL,
  `naskah_v` varchar(200) NOT NULL DEFAULT '0,0,0',
  `durasi_v` varchar(200) NOT NULL,
  `wcr_v` varchar(200) NOT NULL,
  `video_v` varchar(200) NOT NULL DEFAULT '0,0,0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konversi_vektor`
--

INSERT INTO `konversi_vektor` (`id_konversi_vektor`, `id_berita`, `naskah_v`, `durasi_v`, `wcr_v`, `video_v`) VALUES
(1, 1, '0,0,1', '0,1,0', '0,1', '1,0,0'),
(2, 2, '0,0,1', '0,1,0', '0,1', '1,0,0'),
(3, 3, '0,0,1', '0,0.83333333333333,0.16666666666667', '0.5,0.5', '1,0,0'),
(4, 4, '0,0,1', '0,1,0', '0,1', '1,0,0'),
(5, 5, '0,0.5,0.5', '0,1,0', '0,1', '0,1,0'),
(6, 6, '0,0,1', '0,1,0', '0.5,0.5', '0,1,0'),
(7, 7, '0,0,1', '0,1,0', '0.5,0.5', '1,0,0'),
(8, 8, '0,0,1', '0,1,0', '0.5,0.5', '1,0,0'),
(9, 9, '0,0,1', '0,1,0', '0.5,0.5', '1,0,0'),
(10, 10, '0,0.5,0.5', '0,1,0', '0,1', '1,0,0'),
(11, 11, '0,0,1', '0,1,0', '0,1', '1,0,0'),
(12, 12, '0,0,1', '0,1,0', '0.5,0.5', '1,0,0'),
(13, 13, '0,0,1', '0,1,0', '0,1', '0,1,0'),
(14, 14, '0,0,1', '0,1,0', '0.5,0.5', '1,0,0'),
(15, 15, '0,0,1', '0,1,0', '0,1', '1,0,0'),
(16, 16, '0,0,1', '0,1,0', '0.5,0.5', '1,0,0'),
(17, 17, '0,0,1', '0,1,0', '0,1', '1,0,0'),
(18, 18, '0.5,0.5,0', '0,0,1', '1,0', '0,0,1'),
(19, 19, '0,1,0', '0,1,0', '0.5,0.5', '0,0,1'),
(20, 20, '0,1,0', '0,1,0', '0.5,0.5', '0,0,1');

-- --------------------------------------------------------

--
-- Table structure for table `sifat_berita`
--

CREATE TABLE `sifat_berita` (
  `id_sifat` int(4) NOT NULL,
  `nama_sifat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sifat_berita`
--

INSERT INTO `sifat_berita` (`id_sifat`, `nama_sifat`) VALUES
(1, 'Hard News'),
(2, 'Soft News');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sifat_berita`
--

CREATE TABLE `sub_sifat_berita` (
  `id_sub` int(4) NOT NULL,
  `nama_sub_sifat` varchar(100) NOT NULL,
  `id_sifat` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_sifat_berita`
--

INSERT INTO `sub_sifat_berita` (`id_sub`, `nama_sub_sifat`, `id_sifat`) VALUES
(1, 'Razia aparat penegak hukum', 1),
(2, 'Kelangkaan sumberdaya alam/barang pokok', 1),
(3, 'Operasi tangkap tangan (pungli)/KPK', 1),
(4, 'Inpeksi mendadak eksekutif/legislatif', 1),
(5, 'Demonstrasi warga/mahasiswa', 1),
(6, 'Musibah/bencana alam/kecelakaan', 1),
(7, 'Kerusakan fasilitias umum', 1),
(8, 'Peristiwa kriminal', 1),
(9, 'DLL', 1),
(10, 'Suasana Panen', 2),
(11, 'Upacara penting atau kenegaraan', 2),
(12, 'perayaan penting', 2),
(13, 'kegiatan kreatifitas masyarakat', 2),
(14, 'Kegiatan/permainan/upacara tradisional', 2),
(15, 'DLL', 2),
(16, 'kegiatan tokoh penting/aparat/pemerintah', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(4) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `jk` varchar(100) NOT NULL,
  `level` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_lengkap`, `jk`, `level`, `email`) VALUES
(1, 'kontributor', 'kontributor', 'Nanda', 'Perempuan', 'kontributor', 'anandalaks2@gmail.com'),
(2, 'nanda', 'nanda', 'Ananda', 'Laki - Laki', 'produser', 'nanda@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`),
  ADD KEY `berita_ibfk_1` (`id_user`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`id_hasil`);

--
-- Indexes for table `konversi_real`
--
ALTER TABLE `konversi_real`
  ADD PRIMARY KEY (`id_konversi_real`);

--
-- Indexes for table `konversi_vektor`
--
ALTER TABLE `konversi_vektor`
  ADD PRIMARY KEY (`id_konversi_vektor`);

--
-- Indexes for table `sifat_berita`
--
ALTER TABLE `sifat_berita`
  ADD PRIMARY KEY (`id_sifat`);

--
-- Indexes for table `sub_sifat_berita`
--
ALTER TABLE `sub_sifat_berita`
  ADD PRIMARY KEY (`id_sub`),
  ADD KEY `id_sifat` (`id_sifat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `id_hasil` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- AUTO_INCREMENT for table `konversi_real`
--
ALTER TABLE `konversi_real`
  MODIFY `id_konversi_real` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1723;

--
-- AUTO_INCREMENT for table `konversi_vektor`
--
ALTER TABLE `konversi_vektor`
  MODIFY `id_konversi_vektor` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `sifat_berita`
--
ALTER TABLE `sifat_berita`
  MODIFY `id_sifat` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_sifat_berita`
--
ALTER TABLE `sub_sifat_berita`
  MODIFY `id_sub` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sub_sifat_berita`
--
ALTER TABLE `sub_sifat_berita`
  ADD CONSTRAINT `sub_sifat_berita_ibfk_1` FOREIGN KEY (`id_sifat`) REFERENCES `sifat_berita` (`id_sifat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
